var GoogleSpreadsheet = require('google-spreadsheet');
var async = require('async');

// spreadsheet key is the long id in the sheets URL 
var spreadsheetkey = ""
if(process.env.NODE_ENV === 'production')
{
    spreadsheetkey = "1eNA6PbXM4vKeoCpLK4UR4PFQeI6btDjHvYCCbYVE9NU"
}else {
    spreadsheetkey = "1eNA6PbXM4vKeoCpLK4UR4PFQeI6btDjHvYCCbYVE9NU"
}

var doc = new GoogleSpreadsheet(spreadsheetkey);
var sheet;

var googleSpreadSheet = function () {
  var self = this;

  this.printSomething = function () {
    console.log("something")
  }


  this.addRow = function (newCamper) {
    async.series([
      function setAuth(step) {
        // see notes below for authentication instructions! 
        var creds = require('./google-generated-creds.json');
        doc.useServiceAccountAuth(creds, step);
      },
      function getInfoAndWorksheets(step) {
        doc.getInfo(function (err, info) {
          console.log('Loaded doc: ' + info.title + ' by ' + info.author.email);
          sheet = info.worksheets[0];
          // console.log('sheet 1: ' + sheet.title + ' ' + sheet.rowCount + 'x' + sheet.colCount);
          step();
        });
      },
      function workingWithRows(step) {
        // google provides some query options 
        console.log(newCamper)
        doc.addRow(1,newCamper
         , function (err, result) {
          console.log('Added row!');

          step();
        });
      }
    ]);

  }

  this.addEditProfileRow = function (newCamper) {
    async.series([
      function setAuth(step) {
        // see notes below for authentication instructions! 
        var creds = require('./google-generated-creds.json');
        doc.useServiceAccountAuth(creds, step);
      },
      function getInfoAndWorksheets(step) {
        doc.getInfo(function (err, info) {
          console.log('Loaded doc: ' + info.title + ' by ' + info.author.email);
          sheet = info.worksheets[1];
          // console.log('sheet 1: ' + sheet.title + ' ' + sheet.rowCount + 'x' + sheet.colCount);
          step();
        });
      },
      function workingWithRows(step) {
        // google provides some query options 
        console.log(newCamper)
        doc.addRow(1,newCamper
         , function (err, result) {
          console.log('Added row!');

          step();
        });
      }
    ]);

  }

  this.addSupportRow = function (newSupportTicket) {
    async.series([
      function setAuth(step) {
        // see notes below for authentication instructions! 
        var creds = require('./google-generated-creds.json');
        doc.useServiceAccountAuth(creds, step);
      },
      function getInfoAndWorksheets(step) {
        doc.getInfo(function (err, info) {
          console.log('Loaded doc: ' + info.title + ' by ' + info.author.email);
          sheet = info.worksheets[2];
          console.log('sheet 2: ' + sheet.title);
          step();
        });
      },
      function workingWithRows(step) {
        // google provides some query options 
        console.log(newSupportTicket)
        doc.addRow(2,newSupportTicket
         , function (err, result) {
          console.log('Added support ticket!');
          step();
        });
      }
    ]);

  }

  this.addScoreRow = function (newScore) {
    async.series([
      function setAuth(step) {
        // see notes below for authentication instructions! 
        var creds = require('./google-generated-creds.json');
        doc.useServiceAccountAuth(creds, step);
      },
      function getInfoAndWorksheets(step) {
        doc.getInfo(function (err, info) {
          console.log('Loaded doc: ' + info.title + ' by ' + info.author.email);
          sheet = info.worksheets[3];
          console.log('sheet 3: ' + sheet.title);
          step();
        });
      },
      function workingWithRows(step) {
        // google provides some query options 
        console.log(newScore)
        doc.addRow(15,newScore
         , function (err, result) {
          console.log('Added score!');
          step();
        });
      }
    ]);

  }

  this.updatePayment = function (userID,payment) {
    async.series([
      function setAuth(step) {
        // see notes below for authentication instructions! 
        var creds = require('./google-generated-creds.json');
        doc.useServiceAccountAuth(creds, step);
      },
      function getInfoAndWorksheets(step) {
        doc.getInfo(function (err, info) {
          console.log('Loaded doc: ' + info.title + ' by ' + info.author.email);
          sheet = info.worksheets[0];
          console.log('sheet 0: ' + sheet.title);
          step();
        });
      },
      function workingWithRows(step) {
        // google provides some query options 
        sheet.getRows({}, function( err, rows ){
          console.log('Read '+rows.length+' rows');

          for(var i = 0; i < rows.length;i++){

            console.log(rows[i]);
            console.log(rows[i].userid);
            console.log(userID);

            if (rows[i].userid == userID) {
              console.log("ys");
              // the row is an object with keys set by the column headers
              rows[i].paymentStatus = 'paid';
              rows[i].save(); // this is async
            }
          }
    
          step();
          
        });
      }
    ]);

  }
}


module.exports = googleSpreadSheet;