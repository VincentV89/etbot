const Telegraf = require('telegraf')
const TelegrafFlow = require('telegraf-flow')
const express = require('express')
const expressApp = express()
const session = require('telegraf/session')
const { Scene } = TelegrafFlow

var telegramkey = ''
if (process.env.NODE_ENV === 'production') {
  telegramkey = '528487074:AAHlQBx1ITnkUhW_ivor0d5UYWthfjVD7zY'
} else {
  telegramkey = '528487074:AAHlQBx1ITnkUhW_ivor0d5UYWthfjVD7zY'
  // telegramkey = '358050311:AAFzU2r_5UH7p2NcMU5lc6nwG2w4acwbUlI'
}
const app = new Telegraf(telegramkey)
const flow = new TelegrafFlow()
const broadcastScene = new Scene('broadcast')


//"338901098:AAHIN92Ynj4GCAAom9B4231kBPRHeAIDp6M"

const adminMenu = require('./middlewares/adminMenu')
const {
  broadcastMenu,
  addToBroadcastGroup,
  initBroadcastFlow,
  sendBroadcast,
  askBroadcastMsg,
  handleBroadcastContent,
  selectHouses,
  addToBroadcastHouse
} = require('./middlewares/broadcastMenu')
const {
  startRegister,
  editProfile,
  raiseSupport,
  camperMenu,
  sendTrailer,
  sendPackingList,
  sendIntro,
  sendLeaderboard,
  sendInstaLink,
  startDirectChat,
  sendBattle,
  addScore,
  selectEditProfile,
  makePayment,
  finishPayment
} = require('./middlewares/camperMenu')
const { getUser, isAdmin, isRegistered } = require('./helpers/getUser')
// middlewares
app.use(Telegraf.log())
app.use(getUser)

// entry points
flow.action('Broadcast', initBroadcastFlow)
app.command('menu', ctx => camperMenu(ctx))
app.command('admin', ctx => (isAdmin(ctx) ? adminMenu(ctx) : ctx.reply(
  'You are not an admin'
)))
app.command('start', ctx => (isAdmin(ctx) ? adminMenu(ctx) : camperMenu(ctx)))
flow.action('score', (ctx) => ctx.flow.enter('addScore'))

// flows
flow.register(broadcastScene)
flow.register(startRegister)
flow.register(editProfile)
flow.register(raiseSupport)
flow.register(addScore)

// Camper Menu
flow.action('Register', ctx => {
  if (ctx.update.callback_query.message.chat.id < 0) {
    ctx.reply(
      'Yo! Please start a private chat with me @etencounter_bot and register there.'
    )
  } else {
    isRegistered(ctx)
      ? ctx.reply('You are already registered')
      : ctx.flow.enter('startRegister')
  }
})
flow.action('Profile', ctx => {
  isRegistered(ctx)
      ? ctx.flow.enter('editProfile')
      : ctx.reply('You are not registered yet.')
})
app.action('Trailer', sendTrailer)
app.action('PackingList', sendPackingList)
app.action('Intro', sendIntro)
app.action('Leaderboard', sendLeaderboard)
app.action('Connect', sendInstaLink)
app.action('Start', startDirectChat)
app.action('Battle', sendBattle)
app.action('Payment',makePayment)
flow.action('Support', ctx => ctx.flow.enter('raiseSupport'))
app.action('editName', selectEditProfile('name'))
app.action('editNumber', selectEditProfile('number'))
app.action('editAttendance', selectEditProfile('attendance'))
app.action('editAllergy', selectEditProfile('allergy'))


app.on('pre_checkout_query', ({ answerPreCheckoutQuery }) => answerPreCheckoutQuery(true))
app.on('successful_payment', finishPayment)


app.command('start', ctx => {
  ctx.reply(
    'Hello, I am the Camp Encounter Bot. Tab /menu to see what i can do!'
  )
})

// Broadcast Flow
broadcastScene.enter(broadcastMenu)
broadcastScene.action('broadcastCampers', addToBroadcastGroup('Campers'))
broadcastScene.action('broadcastHouses', selectHouses)
broadcastScene.action('broadcastHouseICs', addToBroadcastGroup('HouseICs'))
broadcastScene.action('broadcastHelpers', addToBroadcastGroup('Helpers'))
broadcastScene.action('broadcastCampComm', addToBroadcastGroup('CampComm'))
broadcastScene.action(
  'broadcastTeamLeaders',
  addToBroadcastGroup('TeamLeaders')
)
broadcastScene.action('broadcast:Scylla', addToBroadcastHouse('Scylla'))
broadcastScene.action('broadcast:Hydra', addToBroadcastHouse('Hydra'))
broadcastScene.action('broadcast:Kraken', addToBroadcastHouse('Kraken'))
broadcastScene.action('broadcastHouse:done', broadcastMenu)
broadcastScene.action('broadcast:done', askBroadcastMsg)
broadcastScene.on('text', handleBroadcastContent)
broadcastScene.action('broadcast:yes', sendBroadcast)

app.use(session())
app.use(flow.middleware())

// Set telegram webhook
// app.telegram.setWebhook('https://camp-encounter.appspot.com', {
//   content: 'server-cert.pem'
// })

// // TLS options
// const tlsOptions = {
//   key:  fs.readFileSync('server-key.pem'),
//   cert: fs.readFileSync('server-cert.pem'),
//   ca: [
//     // This is necessary only if the client uses the self-signed certificate.
//     fs.readFileSync('client-cert.pem')
//   ]
// }

// // Start https webhook
// app.startWebhook('/secret-path', null, 8443)

// Http webhook, for nginx/heroku users.
// app.startWebhook('/secret-path', null, 5000)

// // Use webhookCallback() if you want to attach telegraf to existing http server
// require('http')
//   .createServer(app.webhookCallback('/secret-path'))
//   .listen(3000)

// require('https')
//   .createServer(app.webhookCallback('/secret-path'))
//   .listen(8443)

// expressApp.use(app.webhookCallback('secret-path'))
// expressApp.get('/', (req,res) => {
//     res.send('Hello World!')
// })

// expressApp.listen(3000, () => {
//     console.log('Example app is listening on port 3000.')
// })


var port = process.env.PORT || 3000;

/*
app.listen(port, "0.0.0.0", function() {
console.log("Listening on Port 3000");
});*/

app.startPolling('/secret-path', null, port)