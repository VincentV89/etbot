const { Markup, Extra } = require('telegraf')
const TelegrafFlow = require('telegraf-flow')
const GoogleSpreadSheet = require('../google_spread_sheet')
const googleSpreadSheet = new GoogleSpreadSheet()
const database = require('../firebase')
const { WizardScene } = TelegrafFlow
const flow = new TelegrafFlow()
const { getUser, isAdmin, isRegistered, notPaid } = require('../helpers/getUser')
const fs = require('fs');

const camperMenu = ctx => {
  return ctx.reply(
    'Welcome to Camp Encounter Bot Menu\nTap on @etencounter_bot to initiate private chat with me :)',
    Markup.inlineKeyboard([
      [
        Markup.callbackButton('⚡ Connect', 'Connect'),
        Markup.callbackButton('🌟 Intro', 'Intro')
      ],
      [
        Markup.callbackButton('✋ Register', 'Register'),
        Markup.callbackButton('📝 Packing List', 'PackingList')
      ],
      [
        Markup.callbackButton('💰 Payment', 'Payment'),
        Markup.callbackButton('🛎️ Support', 'Support')
      ],
      [
        Markup.callbackButton('🏆 Leaderboard', 'Leaderboard'),
        Markup.callbackButton('📽 Camp Trailer', 'Trailer')
      ],
      [Markup.callbackButton('⚔ Battle', 'Battle')]
      // [
      //   Markup.callbackButton('✋ Register', 'Register')
      // ],
    ]).extra()
  )
}

function validateContact(contact) {
  var re = /^[0-9]{8}$/
  return re.test(contact)
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

function validateAge(age) {
  var re = /^[1-9]?[0-9]{1}$|^100$/
  return re.test(age)
}

function validateName(name) {
  var re = /^[a-zA-Z ]+$/
  return re.test(name)
}

function validateCg(cg) {
  var myarr = [
    'W212',
    'E488',
    'N553',
    'W520',
    'N450',
    'N470',
    'N488',
    'W553',
    'W593',
    'W320',
    'W513',
    'E519',
    'N561'
  ]

  return myarr.indexOf(cg) > -1
}

function checkHouse(cg) {
  var greenArr = ['W520', 'W513', 'W553', 'N488']
  var blueArr = ['N553', 'E519', 'W320', 'N470']
  var redArr = ['W212','E488', 'N450', 'W593','N561']

  if (greenArr.indexOf(cg) > -1) {
    return 'Scylla'
  } else if (blueArr.indexOf(cg) > -1) {
    return 'Hydra'
  } else if (redArr.indexOf(cg) > -1) {
    return 'Kraken'
  }
}

function validateCampStatus(status) {
  var myarr = ['Full', 'Partial']
  return myarr.indexOf(status) > -1
}

function validateTee(tee) {
  var myarr = ['XS', 'S', 'M', 'L', 'XL']
  return myarr.indexOf(tee) > -1
}

function validateYesNo(check) {
  var myarr = ['Yes', 'No']
  return myarr.indexOf(check) > -1
}

const raiseSupport = new WizardScene(
  'raiseSupport',
  ctx => {
    ctx.reply('Please tell us your issue:')
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.issue = ctx.message.text
    ctx.reply('Confirm?', Extra.markup(Markup.keyboard(['Yes', 'No'])))
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.confirmation = ctx.message.text
    if (!validateYesNo(ctx.flow.state.confirmation)) {
      ctx.reply(
        'Please enter Yes or No:',
        Extra.markup(Markup.keyboard(['Yes', 'No']))
      )
      this.selectStep(this.state._pos)
    } else {
      if (ctx.flow.state.confirmation == 'Yes') {
        ctx.reply('Your support ticket is saved!')
        var issue = ctx.flow.state.issue
        var name =
          ctx.update.message.from.first_name +
          ' ' +
          ctx.update.message.from.last_name

        const newTicket = { name, issue }

        googleSpreadSheet.addSupportRow(newTicket)

        ctx.flow.leave()
      } else {
        ctx.reply('No support tickets are raised.')
        ctx.flow.leave()
      }
    }
  }
)

const startRegister = new WizardScene(
  'startRegister',
  ctx => {
    ctx.reply('How should I address you? Full name please!')
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.name = ctx.message.text
    if (!validateName(ctx.flow.state.name)) {
      ctx.reply('Please enter a proper name:')
      this.selectStep(this.state._pos)
    } else {
      ctx.reply('How young are you?')
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.age = ctx.message.text
    if (!validateAge(ctx.flow.state.age)) {
      ctx.reply('Please enter a proper age:')
      this.selectStep(this.state._pos)
    } else {
      ctx.reply(
        'Are you a member?',
        Extra.markup(Markup.keyboard([['Yes!'], ['No, my friend invited me!']]))
      )
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.isMember = ctx.message.text

    if (ctx.flow.state.isMember == 'Yes!') {
      ctx.reply(
        'Great ! Press ok to continue',
        Extra.markup(Markup.keyboard(['Ok']))
      )
      ctx.flow.wizard.next()
    } else {
      ctx.reply(
        'Who invited you?',
        Extra.markup(Markup.removeKeyboard())
      )
      ctx.flow.wizard.next()
    }

    
  },
  ctx => {
    ctx.flow.state.friend = ''

    var message = 'Cool, which cell group is your friend from?'
    if (ctx.flow.state.isMember == 'Yes!') {
      message = 'Cool, which cell group are you from?'
    } else {
      ctx.flow.state.friend = ctx.message.text
    }

    ctx.reply(
      message,
      Extra.markup(
        Markup.keyboard([
          ['W212','E488', 'N553', 'W520','N561'],
          ['N450', 'N470', 'N488', 'W553'],
          ['W593', 'W320', 'W513', 'E519']
        ])
      )
    )
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.cellgroup = ctx.message.text
    if (!validateCg(ctx.flow.state.cellgroup)) {
      ctx.reply(
        'Please enter a cg from the keyboard options:',
        Extra.markup(
          Markup.keyboard([
            ['W212','E488', 'N553', 'W520','N561'],
            ['N450', 'N470', 'N488', 'W553'],
            ['W593', 'W320', 'W513', 'E519']
          ])
        )
      )
      this.selectStep(this.state._pos)
    } else {
      ctx.reply(
        'Are you a full or half camper?',
        Extra.markup(Markup.keyboard(['Full', 'Partial']))
      )
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.fullcamper = ctx.message.text
    if (!validateCampStatus(ctx.flow.state.fullcamper)) {
      ctx.reply(
        'Please enter Full or Partial:',
        Extra.markup(Markup.keyboard(['Full', 'Partial']))
      )
      this.selectStep(this.state._pos)
    } else {
      if (ctx.message.text == 'Partial') {
        ctx.reply(
          'Please indicate the meals which you will be having\n•Day1 Lunch\n•Day1 Dinner\n•Day2 Breakfast\n•Day2 Lunch\n•Day2 Dinner\n•Day3 Breakfast\n•Day3 Lunch',
          Extra.markup(Markup.removeKeyboard())
        )
      } else {
        ctx.reply(
          'Great ! Press ok to continue',
          Extra.markup(Markup.keyboard(['Ok']))
        )
      }
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.food = 'All'
    if (ctx.flow.state.fullcamper == 'Partial') {
      ctx.flow.state.food = ctx.message.text
    }

    const extra = Extra.markup(Markup.keyboard([['XS', 'S'], ['M', 'L'], ['XL']]))
    extra.caption = "Choose your t-shirt size!\n👕 Sizes available - XS to XXXXL\nGood gauge will be:\nXS - Elisa\nS - Diana / Jonathan\nM - Jordan\nL - Yanming\nXL - Lester"

    ctx.replyWithPhoto({ source: './media/shirt_size.jpg' },
      extra
    )
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.teesize = ctx.message.text
    if (!validateTee(ctx.flow.state.teesize)) {
      ctx.reply(
        'Please enter a size from the keyboard options:',
        Extra.markup(Markup.keyboard([['XS', 'S'], ['M', 'L'], ['XL']]))
      )
      this.selectStep(this.state._pos)
    } else {
      ctx.reply(
        'Can I have your Contact Number?',
        Extra.markup(Markup.removeKeyboard())
      )
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.contact = ctx.message.text
    if (!validateContact(ctx.flow.state.contact)) {
      ctx.reply('Please enter a 8-digit contact number:')
      this.selectStep(this.state._pos)
    } else {
      ctx.reply('What is your email address?')
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.email = ctx.message.text
    if (!validateEmail(ctx.flow.state.email)) {
      ctx.reply('Please enter a proper email:')
      this.selectStep(this.state._pos)
    } else {
      ctx.reply('In case of emergency, who can we call?')
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.nok = ctx.message.text
    if (!validateName(ctx.flow.state.nok)) {
      ctx.reply('Please enter a proper name of your next of kin:')
      this.selectStep(this.state._pos)
    } else {
      ctx.reply('How can we reach your Next-of-Kin? ')
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.nokcontact = ctx.message.text
    if (!validateContact(ctx.flow.state.nokcontact)) {
      ctx.reply('Please enter a 8-digit contact number:')
      this.selectStep(this.state._pos)
    } else {
      ctx.reply(
        'Do you have any dietary restrictions?',
        Extra.markup(Markup.keyboard(['Yes', 'No']))
      )
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.foodallergy = ctx.message.text
    if (ctx.flow.state.foodallergy == 'Yes') {
      ctx.reply(
        'What dietary restrictions do you have?',
        Extra.markup(Markup.removeKeyboard())
      )
      this.selectStep(this.state._pos)
    }  else {
      ctx.reply(
          'By submitting this registration form, you agree that Camp Encounter\'18 may collect, use and disclose your personal data, as provided in this registration form, for the following purposes in accordance with the Personal Data Protection Act 2012 and our data protection policy: to administer for the Camp Encounter\'18, including to contact you for the administration in relation to the camp.',
        Extra.markup(Markup.keyboard(['Yes', 'No']))
      )
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.pdpa = ctx.message.text
    if (ctx.flow.state.pdpa == 'No') {
      ctx.reply('Registration flow cancelled, please register again!')
      ctx.flow.leave()
    } else {
      ctx.reply(
        'Please confirm your details:\n•Name: ' +
          ctx.flow.state.name +
          '\n•Age: ' +
          ctx.flow.state.age +
          '\n•Cell Group: ' +
          ctx.flow.state.cellgroup +
          '\n•Camp Status: ' +
          ctx.flow.state.fullcamper +
          '\n•Meals: ' +
          ctx.flow.state.food +
          '\n•Tee: ' +
          ctx.flow.state.teesize +
          '\n•Contact: ' +
          ctx.flow.state.contact +
          '\n•Email: ' +
          ctx.flow.state.email +
          '\n•NoK: ' +
          ctx.flow.state.nok +
          '\n•NoK Contact: ' +
          ctx.flow.state.nokcontact +
          '\n•Food Allergy: ' +
          ctx.flow.state.foodallergy +
          '',
        Extra.markup(Markup.keyboard(['Yes', 'No']))
      )
      ctx.flow.wizard.next()
    }
  },
  ctx => {
    ctx.flow.state.confirmation = ctx.message.text
    var house = checkHouse(ctx.flow.state.cellgroup)
    if (!validateYesNo(ctx.flow.state.confirmation)) {
      ctx.reply(
        'Please enter Yes or No:',
        Extra.markup(Markup.keyboard(['Yes', 'No']))
      )
      this.selectStep(this.state._pos)
    } else {
      if (ctx.flow.state.confirmation == 'No') {
        ctx.reply('Registration flow cancelled, please register again!')
        ctx.flow.leave()
      } else {
        const houseColor = {
          Scylla: 'GREEN',
          Hydra: 'BLUE',
          Kraken: 'RED'
        }
        const baseReply = house =>
          `Congratulations! You are registered! You belong to the elite house of The ${houseColor[house]} ${house}!`
        const houseIntro = `*The House names of "Encounter '18: Rivers" are based on mythical sea creatures, known to be legends of the sea - impregnable fortresses and feared by all.`
        const houseMap = {
          Scylla: `Scylla ("sky-ller")
A man-devouring sea monster of twelve feet and six ferocious heads lined with three rows of sharp teeth each. Deadly, brutal.`,
          Hydra: `Hydra ("hi-dra")
A multi-headed serpent in Greek Mythology believed to have a regeneration feature - for every head cut off, it would grow back a couple more. Immovable, undefeated.`,
          Kraken: `Kraken ("crack-en")
A legendary sea monster of enormous size that is believed to have the ability to pull a large sailing ship underwater with its arms. Fearsome, fortified.`
        }
        ctx.reply(
          '*Congratulations! You are registered! Get yourself ready for a power-packed time at Encounter 2018!',
          Extra.markup(Markup.removeKeyboard())
        )
        var name = ctx.flow.state.name
        var age = ctx.flow.state.age
        var cellgroup = ctx.flow.state.cellgroup
        var fullcamper = ctx.flow.state.fullcamper
        var food = ctx.flow.state.food
        var teesize = ctx.flow.state.teesize
        var contact = ctx.flow.state.contact
        var email = ctx.flow.state.email
        var nok = ctx.flow.state.nok
        var nokcontact = ctx.flow.state.nokcontact
        var foodallergy = ctx.flow.state.foodallergy
        var isMember = ctx.flow.state.isMember
        var friend = ctx.flow.state.friend

        // var house = checkHouse(cellgroup)

        const newCamper = {
          name,
          age,
          cellgroup,
          fullcamper,
          food,
          teesize,
          contact,
          email,
          nok,
          nokcontact,
          foodallergy,
          house,
          friend,
          isMember,
          type: 'camper',
          userId: ctx.message.from.id,
          paymentStatus: 'no'
        }

        googleSpreadSheet.addRow(newCamper)
        database.ref().child(`/registers/${ctx.message.from.id}`).set(newCamper)

        ctx.flow.leave()
      }
    }
  }
)

//T-Shirt,Contact,Email,NOK,NOK Contact,Food Allergy,Are you Vegan?
//fs.createReadStream('/path/to/video.mp4'
//'https://picsum.photos/200/300/'
const sendPackingList = ctx =>
  isRegistered(ctx)
    ? ctx.replyWithPhoto('https://drive.google.com/open?id=1Cjljq9MJUPu1T_Jf-4SNkeljT76-LV_O')
    : ctx.reply('Sorry! Please register first!')
const sendTrailer = ctx =>
  ctx.replyWithVideo({url: 'https://drive.google.com/open?id=1S2PlqGz80lMg0RlXjOXWXfu7L_gcqO2W'})
const sendIntro = ctx => {
  // ctx.replyWithPhoto({ source: './media/intro.jpg' })
  ctx.reply(`🌊ET Camp Encounter 2018🌊

<<Camp Encounter 2018: RIVERS>> checks off all you're looking for in a JUNE YOUTH CAMP!

☑️ Fun & Happening Programmes
☑️ Eternal Friendships Forged
☑️ Deep Encounters with God

⚓WHEN: 8-10 June!
⚓WHERE: The New Charis Mission!
⚓HOW MUCH: $40!

The Encounter '17 experience doesn't just start on 8th June, but NOW!
Introducing the 3 E's leading to Encounter 2018....

⛲ESCAPE (20 May)
🏝EMERGE (28 MAY)
🚤ENGAGE (4 JUN)

For more details, tap on "Connect"!
Cyu there!`)
}
const sendLeaderboard = ctx =>
  isRegistered(ctx)
    ? ctx.reply(
        '🏆LEADER BOARD🏆\n\FINAL PRE-CAMP BATTLE RESULT⚔️\n1️⃣KRAKEN\n2️⃣HYDRA\n3️⃣SCYLLA\n\nKRAKEN Top Players\n1. Eden – 430\n2. savanna – 380\n3. Timothy Tay – 360\n4. ME – 352\n5. Joanna Lye – 352\n6. zhuohannn – 347\n7. Joshua – 345\n8. Gurusharran Gk – 344\n9. Jacob Low – 324\n10. Torance Paul – 319\n11. Rachel – 316\n12. Joseph Peh – 308\n13. Nicholas Tan – 300\n14. xX_Truston609_Xx – 298\n15. DIAN TIAN – 295\n16. ong cong he – 289\n17. Clarence Cheang – 280\n18. Brian – 266\n19. Celine Lim – 257\n20. Jeremiah Ng – 256\n\nHYDRA Top Players\n1. Lee Choon kang – 380\n2. Vincent Oh – 365\n3. Darren Goh – 363\n4. YJ – 354\n5. Mah AnKang – 342\n6. Claria Lim – 327\n7. Sakurauchi Riko – 321\n8. Emelia – 316\n9. Wendy Beth – 312\n10. Gideon – 312\n11. Xianying – 312\n12. Joey Lye – 296\n13. Lichinwong – 296\n14. dougle 🙈 – 278\n15. Ting Huan Leng – 274\n16. Reagan – 272\n17. ZED – 264\n18. Alfred – 262\n19. Huiling AHL – 261\n20. Cheong Kar Jun – 260\n\nSCYLLA Top Players\n1. Nicholas – 387\n2. Elycia Teh – 378\n3. May Fan – 368\n4. Kenric Tay – 365\n5. Sky – 360\n6. rochelle 💫 – 360\n7. Ruth Ng – 326\n8. Arielle; Huisen 🙆 – 322\n9. Lester Chee – 319\n10. Geekinacloset – 316\n11. Jordan Poh – 306\n12. Jinhano – 300\n13. Ziwei Hiew – 294\n14. William Yap – 292\n15. Genevieve – 256\n16. Xinnnyi Tan – 254\n17. Jonathan Tay – 248\n18. yonghui – 236\n19. Weixuan Tien – 232\n20. Rachel – 22\n\The unleash of Camp Encounter 2018 pirates war...\nEnd date : 10 June 1800\nBATTLE FOR GLORY ⚔️!'
      )
    : ctx.reply('Sorry! Please register first!')
const sendInstaLink = ctx => {
  ctx.reply(
    'iOS:\nTap on the link below to connect!\n instagram://user?username=et.encounter'
  )
  ctx.reply(
    'Android:\nTap on the link below to connect!\n http://instagram.com/et.encounter'
  )
}
// const sendBattle = ctx =>
//   isRegistered(ctx)
//     ? ctx.telegram.forwardMessage(
//         ctx.update.callback_query.message.chat.id,
//         189624745,
//         353
//       )
//     : ctx.reply('Sorry! Please register first!')
const sendBattle = ctx =>
  isRegistered(ctx)
    ? ctx.telegram.forwardMessage(
      ctx.update.callback_query.message.chat.id,
      198376758,
      353)
    : ctx.reply('Sorry! Please register first!')

const startDirectChat = ctx => {
  ctx.telegram.sendMessage(
    ctx.update.callback_query.from.id,
    'Hello, I am the Camp Encounter Bot. Tap /menu to see what i can do!'
  )
}

const invoice = {
  provider_token: "284685063:TEST:MmE4M2RlYTFkNzZk",
  start_parameter: 'camp-encounter-sku',
  title: 'Camp Encounter Camper Fee',
  description: 'Ready for the biggest and most fun camp this summer? Battle it out with fellow warriors through the upcoming 3 days of fun & excitement! Order your ticket today!',
  currency: 'sgd',
  photo_url: 'https://img.clipartfest.com/5a7f4b14461d1ab2caaa656bcee42aeb_future-me-fredo-and-pidjin-the-webcomic-time-travel-cartoon_390-240.png',
  prices: [
    { label: 'Camp Fee', amount: 4500 }
  ],
  payload: {
    coupon: 'BLACK FRIDAY'
  }
}

const replyOptions = Markup.inlineKeyboard([
  Markup.payButton('💸 Buy'),
  Markup.urlButton('❤️', 'http://telegraf.js.org')
]).extra()
//ctx.update.callback_query.message.chat.id,
const makePayment = ctx =>
isRegistered(ctx)
  ? (notPaid(ctx) ? ctx.replyWithInvoice(invoice) : ctx.reply('Thanks for trying to make payment but you have already paid.'))
  : ctx.reply('Sorry! Please register first!')

const finishPayment = ctx => {
  console.log('Woohoo')
  database.ref().child(`/registers/${ctx.message.from.id}`).update({paymentStatus:"paid"})

  googleSpreadSheet.updatePayment(ctx.message.from.id,"paid")

}


var editedProfile = { name: false, number: false, attendance: false, food: false }

const editName = ctx =>
  editProfile.name = true

const profileOptions = () => {
  console.log(editedProfile)

  var nameLabel = editedProfile.name ? 'Name ✅' : 'Name'
  var numberLabel = editedProfile.number ? 'Number ✅' : 'Number'
  var attendanceLabel = editedProfile.attendance ? 'Camp Attendance ✅' : 'Camp Attendance'
  var allergyLabel = editedProfile.food ? 'Food Allergy ✅' : 'Food Allergy'
  console.log(nameLabel)

  return Markup.inlineKeyboard([
    [
      Markup.callbackButton(nameLabel, 'editName'),
      Markup.callbackButton(numberLabel, 'editNumber')
    ],
    [
      Markup.callbackButton(attendanceLabel, 'editAttendance'),
      Markup.callbackButton(allergyLabel, 'editAllergy')
    ],
    [Markup.callbackButton('Done', 'broadcast:done')]
  ]).extra()
}

const editProfile = new WizardScene(
  'editProfile',
  ctx => {
    ctx.editMessageText(
      'Please choose profile to edit:',profileOptions()
    )
    ctx.flow.wizard.next()
  }
)

const selectEditProfile = selectedAttribute => ctx => {

  console.log('a')
  if (selectedAttribute == 'name') {
    editedProfile.name = !editedProfile.name
  } else if (selectedAttribute == 'number') {
    editedProfile.number = !editedProfile.number
  } else if (selectedAttribute == 'attendance') {
    editedProfile.attendance = !editedProfile.attendance
  } else if (selectedAttribute == 'allergy') {
    editedProfile.allergy = !editedProfile.allergy
  } else {
    ctx.flow.leave()
  }
    ctx.editMessageText(
      'Please choose profile to edit:',profileOptions()
    )
    ctx.flow.wizard.next()

}

const addScore = new WizardScene(
  'addScore',
  ctx => {
    ctx.reply(
        'Please choose house to assign score:',
        Extra.markup(Markup.keyboard(['Kraken', 'Hydra','Scylla']))
      )
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.house = ctx.message.text
    ctx.reply(
        'Please choose the day for the score:',
        Extra.markup(Markup.keyboard(['Day 1', 'Day 2','Day 3'])
        ))
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.day = ctx.message.text

    var segments = []

    if (ctx.flow.state.day == 'Day 1'){
      segments = ["Walk the Plank","Treasure Quest","Encounter Pirates League","Beach Wars","Bonus"]
    }else if (ctx.flow.state.day == 'Day 2'){
      segments = ["Voyagers of the Seas","Water Aerobics","Battle of the Open Seas","Encounter Ship Race","Taken","Bonus"]
    }else if (ctx.flow.state.day == 'Day 3'){
      segments = ["Encounter Conquest","Attack of the Ships","Bonus"]
    }

    ctx.reply(
        'Please choose the segment:',
        Extra.markup(Markup.keyboard(segments)
        ))
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.segment = ctx.message.text
    ctx.reply('Please choose score to assign:', 
      Extra.markup(Markup.keyboard([['20', '30','50'],
      ['100', '200','300'],
      ['500', '1000']]))
    )
    ctx.flow.wizard.next()
  },
  ctx => {
    ctx.flow.state.score = ctx.message.text

    ctx.reply(
        'Please confirm the details:\n•House: ' +
          ctx.flow.state.house +
          '\n•Day: ' + ctx.flow.state.day +
          '\n•Segment: ' + ctx.flow.state.segment +
          '\n•Assigned Score: ' +
          ctx.flow.state.score,
        Extra.markup(Markup.keyboard(['Yes', 'No']))
      )
      ctx.flow.wizard.next()
    
  },
  ctx => {

    if (ctx.message.text == 'Yes'){

    var house = ctx.flow.state.house
    var day = ctx.flow.state.day
    var segment = ctx.flow.state.segment
    var score = ctx.flow.state.score

    var name =
          ctx.update.message.from.first_name +
          ' ' +
          ctx.update.message.from.last_name

    const newScore = {
          house,
          day,
          segment,
          score,
          name
        }

        ctx.reply(
        'Score added!',
        Extra.markup(Markup.removeKeyboard())
      )

        

        googleSpreadSheet.addScoreRow(newScore)
        ctx.flow.leave()
    }else{
      ctx.reply(
        'Score adding aborted!',
        Extra.markup(Markup.removeKeyboard()))
      ctx.flow.leave()
    }
  }
)


module.exports = {
  startRegister,
  editProfile,
  raiseSupport,
  camperMenu,
  sendTrailer,
  sendPackingList,
  sendIntro,
  sendLeaderboard,
  sendInstaLink,
  startDirectChat,
  sendBattle,
  addScore,
  selectEditProfile,
  makePayment,
  finishPayment
}
