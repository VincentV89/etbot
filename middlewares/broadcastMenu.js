const { Markup } = require('telegraf')
const database = require('../firebase')

const broadcastOptions = () => {
  return Markup.inlineKeyboard([
    [
      Markup.callbackButton('Campers', 'broadcastCampers'),
      Markup.callbackButton('Houses', 'broadcastHouses')
    ],
    [
      Markup.callbackButton('House ICs', 'broadcastHouseICs'),
      Markup.callbackButton('Team Leaders', 'broadcastTeamLeaders')
    ],
    [
      Markup.callbackButton('Helpers', 'broadcastHelpers'),
      Markup.callbackButton('Camp Comm', 'broadcastCampComm')
    ],
    [Markup.callbackButton('Done', 'broadcast:done')]
  ]).extra()
}

const houseOptions = () => {
  return Markup.inlineKeyboard([
    [
      Markup.callbackButton('Scylla', 'broadcast:Scylla'),
      Markup.callbackButton('Hydra', 'broadcast:Hydra'),
      Markup.callbackButton('Kraken', 'broadcast:Kraken')
    ],
    [Markup.callbackButton('Done', 'broadcastHouse:done')]
  ]).extra()
}
const initState = ctx => {
  if (!ctx.flow.state.list) {
    ctx.flow.state.list = []
    ctx.flow.state.houses = []
  }
}
const selectHouses = ctx => {
  const { list, houses } = ctx.flow.state
  const msg = houses.length > 0
    ? `Group selected: ${getHouseAndGroupText(ctx, houses, list)}`
    : 'Select houses'
  return ctx.editMessageText(msg, houseOptions())
}
const broadcastMenu = ctx => {
  initState(ctx)
  const { list, houses } = ctx.flow.state
  const msg = list.length > 0 || houses.length > 0
    ? `Group selected: ${getHouseAndGroupText(ctx, houses, list)}`
    : 'Select group to broadcast to'
  return ctx.editMessageText(msg, broadcastOptions())
}
const getBroadcastGroup = ctx =>
  ctx.flow.state.list.reduce((r, v) => `${r}, ${v}`)
const getBroadcastHouse = ctx =>
  ctx.flow.state.houses.reduce((r, v) => `${r}, ${v}`)
const getHouseAndGroupText = (ctx, houses, list) => {
  const houseSelected = houses.length > 0 ? getBroadcastHouse(ctx) : ''
  const groupSelected = list.length > 0 ? getBroadcastGroup(ctx) : ''
  const fullMsg = houses.length > 0
    ? `${groupSelected}, Houses(${houseSelected})`
    : `${groupSelected}`
  return fullMsg[0] === ',' ? fullMsg.substring(1) : fullMsg
}
const addToBroadcastGroup = broadcastGroup => ctx => {
  const { list, houses } = ctx.flow.state
  if (!list.includes(broadcastGroup)) list.push(broadcastGroup)
  ctx.editMessageText(
    `Group selected: ${getHouseAndGroupText(ctx, houses, list)}`,
    broadcastOptions()
  )
}
const addToBroadcastHouse = broadcastHouse => ctx => {
  const { list, houses } = ctx.flow.state
  if (!houses.includes(broadcastHouse)) houses.push(broadcastHouse)
  ctx.editMessageText(
    `Group selected: ${getHouseAndGroupText(ctx, houses, list)}`,
    houseOptions()
  )
}
const initBroadcastFlow = ctx => ctx.flow.enter('broadcast')
const isAnyGroupSelected = ctx =>
  ctx.flow.state.list.length > 0 || ctx.flow.state.houses.length > 0
const askBroadcastMsg = ctx =>
  isAnyGroupSelected(ctx)
    ? ctx.reply('What do you want to broadcast?')
    : ctx.reply(
        'You have not selected any group. Please selected at least one.'
      )
const handleBroadcastContent = ctx => {
  if (isAnyGroupSelected(ctx)) {
    ctx.flow.state.broadcastMsg = ctx.message.text
    console.log(ctx.message.text)
    const { list, houses } = ctx.flow.state
    const groupSelected = getHouseAndGroupText(ctx, houses, list)
    const msg = `
      You are broadcasting to the following group: _${groupSelected}_
    \nWith the following content:
    \n\`\`\` ${ctx.message.text}\`\`\`
    \n *Are you sure?*`
    return ctx.replyWithMarkdown(
      msg,
      Markup.inlineKeyboard([
        [
          Markup.callbackButton('Yes! Please send it!', 'broadcast:yes'),
          Markup.callbackButton('No! I want to make changes', 'Broadcast')
        ]
      ]).extra()
    )
  }
  ctx.reply('You have not selected any group. Please selected at least one.')
}
const buildList = (listOfUsers, groupSelected) => {
  const currentGrp = groupSelected.pop()
  const currentUsers = listOfUsers.filter(x => x.type === currentGrp)
  return groupSelected.length === 0
    ? currentUsers
    : [...buildList(listOfUsers, groupSelected), ...currentUsers]
}

const houseChatId = {
  Scylla: '-1001102891218',
  Hydra: '-1001126521572',
  Kraken: '-1001110573941'
}
const buildHouse = (listOfUsers, houseSelected) => {
  return houseSelected.map(house => ({ userId: houseChatId[house] }))
  // const currentHouse = houseSelected.pop()
  // const currentUsers = listOfUsers.filter(x => x.house === currentHouse)
  // return houseSelected.length === 0
  //   ? currentUsers
  //   : [...buildList(listOfUsers, houseSelected), ...currentUsers]
}
const convertObjToArray = obj => {
  const newArray = []
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) newArray.push(obj[key])
  }
  return newArray
}
const sendBroadcast = async ctx => {
  const camperRef = database.ref(`/registers`)
  const { list, houses } = ctx.flow.state
  const users = await camperRef.once('value').then(s => s.val())
  const newUsersArray = convertObjToArray(users)
  const listToSend = buildList(newUsersArray, list)
  const houseToSend = houses.length ? buildHouse(newUsersArray, houses) : []
  const fullList = [...listToSend, ...houseToSend]
  fullList.forEach(x =>
    ctx.telegram.sendMessage(x.userId, ctx.flow.state.broadcastMsg)
  )
  ctx.reply('Your message have been broadcasted!')
  ctx.flow.leave()
}

module.exports = {
  broadcastMenu,
  addToBroadcastGroup,
  initBroadcastFlow,
  sendBroadcast,
  askBroadcastMsg,
  handleBroadcastContent,
  selectHouses,
  addToBroadcastHouse
}
