const {Markup} = require('telegraf')

const adminMenu = (ctx) => {
  return ctx.reply('Admin Menu',
    Markup.inlineKeyboard([
      [
        Markup.callbackButton('📣 Broadcast', 'Broadcast'),
        Markup.callbackButton('Pin Message', 'pinMessage')
      ],
      [
        Markup.callbackButton('Poll', 'poll'),
        Markup.callbackButton('OOS', 'oos')
      ],
      [
        Markup.callbackButton('Where should I be next?', 'nextLocation'),
        Markup.callbackButton('Assign Score', 'score')
      ]
    ]).extra()
  )
}

module.exports = adminMenu
