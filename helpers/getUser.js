const database = require('../firebase')

const getUser = async (ctx, next) => {
  database.ref(`/registers/${ctx.from.id}`)
  .once('value')
  .then(s => {
    ctx.state.user = s.val()
    next()
  })
}
const isAdmin = (ctx) => ctx.state.user && ctx.state.user.type === 'admin'
const isRegistered = (ctx) => !!ctx.state.user
const notPaid = (ctx) => ctx.state.user.paymentStatus === "none"
module.exports = {
  getUser,
  isAdmin,
  isRegistered,
  notPaid
}
