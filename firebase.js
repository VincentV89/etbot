const firebase = require('firebase')

var firebaseConfig = {}
if(process.env.NODE_ENV === 'production')
{
    firebaseConfig = {
      apiKey: 'AIzaSyANV6gO7PAiJe_lHgWwLJotKHXyeoNjEt4',
      authDomain: 'etencounter17.firebaseapp.com',
      databaseURL: 'https://etencounter17.firebaseio.com/',
      storageBucket: 'etencounter17.appspot.com'
    }
}else {
  firebaseConfig = {
    apiKey: 'AIzaSyANV6gO7PAiJe_lHgWwLJotKHXyeoNjEt4',
    authDomain: 'etencounter17.firebaseapp.com',
    databaseURL: 'https://etencounter17.firebaseio.com/',
    storageBucket: 'etencounter17.appspot.com'
  }
    /*firebaseConfig = {
      apiKey: 'AIzaSyBAVfGWgV1U02jveKE61qrqnDPqSfRJtpc',
      authDomain: 'wellness-tracker.firebaseapp.com',
      databaseURL: 'https://wellness-tracker.firebaseio.com/',
      storageBucket: 'wellness-tracker.appspot.com'
    }*/
}
const config = firebaseConfig
firebase.initializeApp(config)
const database = firebase.database()

module.exports = database
